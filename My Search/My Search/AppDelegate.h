//
//  AppDelegate.h
//  My Search
//
//  Created by Ilija Svilar on 7/6/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

