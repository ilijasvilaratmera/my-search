//
//  ImageViewController.m
//  My Search
//
//  Created by Ilija Svilar on 7/7/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "ImageViewController.h"
#import "Manager+ImageLoader.h"

@interface ImageViewController () <UIGestureRecognizerDelegate>

@end

@implementation ImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecognized:)];

    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    [[Manager sharedInstance] loadAsynchroniouslyImageFromUrl:self.imageurl withCompletionHandler:^(UIImage *loadedImage) {
        self.fullScreenImageView.image = loadedImage;
    } errorHandler:^(NSError *error) {
        
    }];
        // Do any additional setup after loading the view.
}

-(void)tapRecognized:(UITapGestureRecognizer *)gesture{
 
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
