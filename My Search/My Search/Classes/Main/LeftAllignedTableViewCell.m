//
//  LeftAllignedTableViewCell.m
//  My Search
//
//  Created by Ilija Svilar on 7/7/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "LeftAllignedTableViewCell.h"

@implementation LeftAllignedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)prepareForReuse {
    
    self.leftAllignedImageView.image = nil;

}



-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    
    
    if(CGRectContainsPoint(self.leftAllignedImageView.frame, point)) {
        return YES;
    }
    return NO;
    
}


@end
