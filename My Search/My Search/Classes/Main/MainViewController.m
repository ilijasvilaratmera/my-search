 //
//  MainViewController.m
//  My Search
//
//  Created by Ilija Svilar on 7/6/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "MainViewController.h"
#import "LeftAllignedTableViewCell.h"
#import "RightAllignedTableViewCell.h"
#import "Manager.h"
#import "Manager+Communication.h"
#import "Manager+ImageLoader.h"
#import "ItunesSearchResult.h"
#import "GitHubSearchResult.h"
#import "CustomTransitionAnimation.h"
#import "ImageViewController.h"
#import "AlertBox.h"

@interface MainViewController () <UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UIViewControllerTransitioningDelegate>

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property CGRect openingFrame;
@property (strong, nonatomic) NSIndexPath *selectedIndex;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.searchBar.delegate = self;
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner.center = self.view.center;
    [self.view addSubview:self.spinner];
    [self.view bringSubviewToFront:self.spinner];
    [self.spinner setHidden:YES];
    self.mainTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [Manager sharedInstance].searchResultsArray.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (self.segmentedControl.selectedSegmentIndex) {
        case 0:
            {
                
              ItunesSearchResult *result = [Manager sharedInstance].searchResultsArray[indexPath.row];
             if (indexPath.row % 2 == 0) {
                 RightAllignedTableViewCell *rightAllignedCell = (RightAllignedTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"rightAllignedCellIdentifier"];
                 
                 rightAllignedCell.rightAllignedTopLabel.text =  result.title;
                 [[Manager sharedInstance] loadAsynchroniouslyImageFromUrl:result.imageUrl withCompletionHandler:^(UIImage *loadedImage) {
                     RightAllignedTableViewCell *rightAllignedCell = [self.mainTableView cellForRowAtIndexPath:indexPath];
                     if(rightAllignedCell) {
                         rightAllignedCell.rightAllignedImageView.image = loadedImage;
                     }
                     
                 } errorHandler:^(NSError *error) {
                     NSLog(@"error %@", error);
                 }];
                 return (RightAllignedTableViewCell *)rightAllignedCell;

                
                 
              } else {
                 
                  LeftAllignedTableViewCell *leftAllignedCell = (LeftAllignedTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"leftAllignedCellIdentifier"];
                  
                  leftAllignedCell.leftAllignedTopLabel.text = result.title;
                  [[Manager sharedInstance] loadAsynchroniouslyImageFromUrl:result.imageUrl withCompletionHandler:^(UIImage *loadedImage) {
                      
                      LeftAllignedTableViewCell *leftAllignedCell = [self.mainTableView cellForRowAtIndexPath:indexPath];
                      if(leftAllignedCell) {
                          leftAllignedCell.leftAllignedImageView.image = loadedImage;
                      }
                  } errorHandler:^(NSError *error) {
                      NSLog(@"error %@", error);
                  }];
                  
                  return (LeftAllignedTableViewCell *)leftAllignedCell;
                    
    
                }
            }
            break;
        
        case 1:
            {
                
             GitHubSearchResult*result = [Manager sharedInstance].searchResultsArray[indexPath.row];
                if (indexPath.row % 2 == 0) {
                   
                    LeftAllignedTableViewCell *leftAllignedCell = (LeftAllignedTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"leftAllignedCellIdentifier"];
                    
                    leftAllignedCell.leftAllignedTopLabel.text = result.loginName;
                    [[Manager sharedInstance] loadAsynchroniouslyImageFromUrl:result.iconUrl withCompletionHandler:^(UIImage *loadedImage) {
                        
                        LeftAllignedTableViewCell *leftAllignedCell = [self.mainTableView cellForRowAtIndexPath:indexPath];
                        if(leftAllignedCell) {
                            leftAllignedCell.leftAllignedImageView.image = loadedImage;
                        }
                    } errorHandler:^(NSError *error) {
                        NSLog(@"error %@", error);
                    }];
                    
                    return (LeftAllignedTableViewCell *)leftAllignedCell;
                    
                    
                } else {
                    RightAllignedTableViewCell *rightAllignedCell = (RightAllignedTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"rightAllignedCellIdentifier"];
                    rightAllignedCell.rightAllignedTopLabel.text =  result.loginName;
                    [[Manager sharedInstance] loadAsynchroniouslyImageFromUrl:result.iconUrl withCompletionHandler:^(UIImage *loadedImage) {
                        RightAllignedTableViewCell *rightAllignedCell = [self.mainTableView cellForRowAtIndexPath:indexPath];
                        if(rightAllignedCell) {
                            rightAllignedCell.rightAllignedImageView.image = loadedImage;
                        }
                        
                    } errorHandler:^(NSError *error) {
                        NSLog(@"error %@", error);
                    }];
                    
                    return (RightAllignedTableViewCell *)rightAllignedCell;
                    
                   
                    
                    
    
                }
      
            }
            break;
            
        default:
            return nil;
            break;
    }
    
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0f;
}


- (IBAction)segmentedControlValueChanged:(UISegmentedControl *)sender {
    
    [self changeVisibleTab];
    
}



-(void)changeVisibleTab {
    self.mainTableView.dataSource = nil;
    [[Manager sharedInstance] prepareForTabChange];
    [self.mainTableView reloadData];
    [self.searchBar resignFirstResponder];
    [self.searchBar setShowsCancelButton:NO animated:YES];
    self.searchBar.text = @"";
    [self.spinner stopAnimating];
    [self.spinner setHidden:YES];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.mainTableView deselectRowAtIndexPath:indexPath animated:NO];
    switch (self.segmentedControl.selectedSegmentIndex) {
        case 0:
        {
            
            ItunesSearchResult *result = [Manager sharedInstance].searchResultsArray[indexPath.row];
            if (indexPath.row % 2 == 0) {
                self.selectedIndex = indexPath;
                RightAllignedTableViewCell *rightAllignedCell = (RightAllignedTableViewCell *)[self.mainTableView cellForRowAtIndexPath:indexPath];
                self.openingFrame = [rightAllignedCell convertRect:rightAllignedCell.rightAllignedImageView.frame toView:self.view];
                ImageViewController *ivc = [self.storyboard instantiateViewControllerWithIdentifier:@"ImageVC"];
                ivc.imageurl = result.imageUrl;
                ivc.transitioningDelegate = self;
                [self presentViewController:ivc animated:YES completion:nil];
                return;
  
            } else {
                self.selectedIndex = indexPath;
                LeftAllignedTableViewCell *leftAllignedCell = (LeftAllignedTableViewCell *)[self.mainTableView cellForRowAtIndexPath:indexPath];
                self.openingFrame = [leftAllignedCell convertRect:leftAllignedCell.leftAllignedImageView.frame toView:self.view];
                ImageViewController *ivc = [self.storyboard instantiateViewControllerWithIdentifier:@"ImageVC"];
                ivc.imageurl = result.imageUrl;
                ivc.transitioningDelegate = self;
                 [self presentViewController:ivc animated:YES completion:nil];
                return ;
            }
        }
            break;
            
        case 1:
        {
            
            GitHubSearchResult *result = [Manager sharedInstance].searchResultsArray[indexPath.row];
            if (indexPath.row % 2 == 0) {
                self.selectedIndex = indexPath;
                LeftAllignedTableViewCell *leftAllignedCell = (LeftAllignedTableViewCell *)[self.mainTableView cellForRowAtIndexPath:indexPath];
                
                self.openingFrame = [leftAllignedCell convertRect:leftAllignedCell.leftAllignedImageView.frame toView:self.view];
                ImageViewController *ivc = [self.storyboard instantiateViewControllerWithIdentifier:@"ImageVC"];
                ivc.imageurl = result.iconUrl;
                ivc.transitioningDelegate = self;
                [self presentViewController:ivc animated:YES completion:nil];
                return;
            } else {
                self.selectedIndex = indexPath;
                RightAllignedTableViewCell *rightAllignedCell = (RightAllignedTableViewCell *)[self.mainTableView cellForRowAtIndexPath:indexPath];
                self.openingFrame = [rightAllignedCell convertRect:rightAllignedCell.rightAllignedImageView.frame toView:self.view];
                ImageViewController *ivc = [self.storyboard instantiateViewControllerWithIdentifier:@"ImageVC"];
                ivc.imageurl = result.iconUrl;
                ivc.transitioningDelegate = self;
                [self presentViewController:ivc animated:YES completion:nil];
                return;
            }
            
        }
            break;
            
        default:
           return;
            break;
    }


}


#pragma mark SearchBar Delegate Methods


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [self.searchBar resignFirstResponder];
    [self.view endEditing:YES];
    [self.searchBar setShowsCancelButton:NO animated:YES];
    [self searchWithTerms:self.searchBar.text];
    
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar setText:@""];
    [self.searchBar resignFirstResponder];
    [self.searchBar setShowsCancelButton:NO animated:YES];
    
}

//-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
//{
//    return YES;
//}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:YES animated:YES];
    UITextField *searchBarTextField = nil;
    for (UIView *subview in self.searchBar.subviews)
    {
        if ([subview isKindOfClass:[UITextField class]])
        {
            searchBarTextField= (UITextField *)subview;
            break;
        }
    }
    searchBarTextField.enablesReturnKeyAutomatically = NO;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
    [self.searchBar resignFirstResponder];
    [self.view endEditing:YES];
}


-(void)searchWithTerms:(NSString *)searchTerms {
    self.mainTableView.dataSource = self;
    switch (self.segmentedControl.selectedSegmentIndex) {
        case 0:
            {
                [self.spinner setHidden:NO];
                [self.spinner startAnimating];
                [[Manager sharedInstance] searchItunesAsynchroniouslyWithSearchTerm:searchTerms withCompletionHandler:^(NSMutableArray *searchResults) {
                    [self.mainTableView reloadData];
                    [self.spinner stopAnimating];
                    [self.spinner setHidden:YES];
                    if([Manager sharedInstance].searchResultsArray.count == 0) {
                        [AlertBox showAlertWithMessage:@"No search results"
                                      inViewController:self];
                    }
                } errorHandler:^(NSError *error) {
                    [self.spinner stopAnimating];
                    [self.spinner setHidden:YES];
                    NSLog(@"error %@",error.description);
                    [AlertBox showAlertWithMessage:@"Error searching iTunes"
                                  inViewController:self];
                }];
            
            
            }
            break;
            
        case 1:
            {
                 [self.spinner setHidden:NO];
                [self.spinner startAnimating];
                [[Manager sharedInstance] searchGithubAsynchroniouslyWithSearchTerm:searchTerms withCompletionHandler:^(NSMutableArray *searchResults) {
                    [self.mainTableView reloadData];
                    [self.spinner stopAnimating];
                    [self.spinner setHidden:YES];
                    if([Manager sharedInstance].searchResultsArray.count == 0) {
                        [AlertBox showAlertWithMessage:@"No search results"
                                      inViewController:self];
                    }
                } errorHandler:^(NSError *error) {
                    [self.spinner stopAnimating];
                    [self.spinner setHidden:YES];
                    [AlertBox showAlertWithMessage:@"Error searching GitHub"
                                  inViewController:self];
                    NSLog(@"error %@",error.description);
                }];
                

                
            }
            break;
            
        default:
            break;
    }
    
    
    
}




#pragma mark ViewController AnimatedTransition Delegate Methods

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {


    CustomTransitionAnimation *animator = [CustomTransitionAnimation new];
    animator.presenting = YES;
    animator.openingFrame = self.openingFrame;
    return animator;

}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {


    CustomTransitionAnimation *animator = [CustomTransitionAnimation new];
    animator.presenting = NO;
    animator.openingFrame = self.openingFrame;
    return animator;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
