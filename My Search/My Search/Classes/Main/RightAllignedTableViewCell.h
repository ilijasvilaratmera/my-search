//
//  RightAllignedTableViewCell.h
//  My Search
//
//  Created by Ilija Svilar on 7/7/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RightAllignedTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *rightAllignedImageView;
@property (weak, nonatomic) IBOutlet UILabel *rightAllignedTopLabel;

@end
