//
//  RightAllignedTableViewCell.m
//  My Search
//
//  Created by Ilija Svilar on 7/7/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "RightAllignedTableViewCell.h"

@implementation RightAllignedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)prepareForReuse {
    
    self.rightAllignedImageView.image = nil;

}

-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    
    
    if(CGRectContainsPoint(self.rightAllignedImageView.frame, point)) {
        return YES;
    }
    return NO;
    
}

@end
