//
//  LeftAllignedTableViewCell.h
//  My Search
//
//  Created by Ilija Svilar on 7/7/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftAllignedTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *leftAllignedImageView;
@property (weak, nonatomic) IBOutlet UILabel *leftAllignedTopLabel;

@end
