//
//  GitHubSearchResult.m
//  My Search
//
//  Created by Ilija Svilar on 7/6/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "GitHubSearchResult.h"

@implementation GitHubSearchResult


NSString *const kIconUrl = @"avatar_url";
NSString *const kLoginName = @"login";
NSString *const kLinkToUserAccount = @"url";


-(instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if(self != nil) {
        if([dict objectForKey:kLoginName] && ![[dict objectForKey:kLoginName] isKindOfClass:[NSNull class]]) {
            _loginName = [dict objectForKey:kLoginName];
        }
        
        if([dict objectForKey:kLinkToUserAccount] && ![[dict objectForKey:kLinkToUserAccount] isKindOfClass:[NSNull class]]) {
            _linkToUserAccount = [dict objectForKey:kLinkToUserAccount];
        }
        
        if([dict objectForKey:kIconUrl] && ![[dict objectForKey:kIconUrl] isKindOfClass:[NSNull class]]) {
            
            _iconUrl = [dict objectForKey:kIconUrl];
        }
        
        
    }
    return self;
    
}

@end
