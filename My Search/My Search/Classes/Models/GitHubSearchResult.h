//
//  GitHubSearchResult.h
//  My Search
//
//  Created by Ilija Svilar on 7/6/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GitHubSearchResult : NSObject

@property (copy, nonatomic) NSString *iconUrl;
@property (copy, nonatomic) NSString *loginName;
@property (copy, nonatomic) NSString *linkToUserAccount;

-(instancetype)initWithDictionary:(NSDictionary *)dict;

@end
