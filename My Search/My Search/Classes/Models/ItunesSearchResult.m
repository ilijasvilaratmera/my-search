//
//  ItunesSearchResult.m
//  My Search
//
//  Created by Ilija Svilar on 7/6/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "ItunesSearchResult.h"

@implementation ItunesSearchResult

NSString *const kAuthor = @"artistName";
NSString *const kTitle = @"trackName";
NSString *const kImageUrl = @"artworkUrl100";


-(instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if(self != nil) {
        if([dict objectForKey:kAuthor] && ![[dict objectForKey:kAuthor] isKindOfClass:[NSNull class]]) {
            _authorName = [dict objectForKey:kAuthor];
        }
        
        if([dict objectForKey:kTitle] && ![[dict objectForKey:kTitle] isKindOfClass:[NSNull class]]) {
            _title = [dict objectForKey:kTitle];
        }
        
        if([dict objectForKey:kImageUrl] && ![[dict objectForKey:kImageUrl] isKindOfClass:[NSNull class]]) {
            
            _imageUrl = [dict objectForKey:kImageUrl];
        }
        
        
    }
    return self;
    
}

@end
