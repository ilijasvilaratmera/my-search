//
//  ItunesSearchResult.h
//  My Search
//
//  Created by Ilija Svilar on 7/6/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ItunesSearchResult : NSObject

@property (copy, nonatomic) NSString *imageUrl;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *authorName;

-(instancetype)initWithDictionary:(NSDictionary *)dict;

@end
