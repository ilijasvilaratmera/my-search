//
//  Manager.h
//  My Search
//
//  Created by Ilija Svilar on 7/6/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Manager : NSObject

@property (strong, nonatomic, readonly) NSMutableArray *searchResultsArray;
@property (strong, nonatomic) NSCache *imageCache;


+(Manager *)sharedInstance;
-(void)prepareForTabChange;

@end
