//
//  Manager+Communication.m
//  My Search
//
//  Created by Ilija Svilar on 7/6/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "Manager+Communication.h"
#import "ItunesSearchResult.h"
#import "GitHubSearchResult.h"

@implementation Manager (Communication)

NSString *const kItunesUrl = @"https://itunes.apple.com/search";
NSString *const kGithubUrl = @"https://api.github.com/search/users";
NSInteger const kHttpRequestOCode = 200;
NSString *const kHttpRequestErrorDescriptionFormat = @"HTTP Request failed with status code: %d (%@)";
NSString *const kHttpRequestErrorDomain = @"HTTP Request";
NSInteger const kHttpRequestErrorCode = -1000;
NSInteger const kNoDataErrorCode = -1100;
NSString *const kNoDataDescription = @"No data available";

-(void)searchItunesAsynchroniouslyWithSearchTerm:(NSString *)searchTerm withCompletionHandler:(void (^)(NSMutableArray *))completionBlock errorHandler:(void (^)(NSError *))errorBlock {
    
    NSString *parameters = @"";
    NSArray *searchTerms = [searchTerm componentsSeparatedByString:@" "];
    for (NSString *string in searchTerms) {
        if([parameters isEqualToString:@""]) {
            parameters = [parameters stringByAppendingString:[NSString stringWithFormat:@"?limit=200&term=%@",string]];
        } else {
            parameters = [parameters stringByAppendingString:[NSString stringWithFormat:@"+%@",string]];
        }
    }
    NSString *encodedSearchString = [parameters stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kItunesUrl,encodedSearchString];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if(error != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(errorBlock) {
                    errorBlock(error);
                }
            });
        } else {
            if(data) {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                NSRange range = [response.MIMEType rangeOfString:@"text/javascript"];
                if(httpResponse.statusCode == 200 && range.length != 0) {
                    NSError *error;
                    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                    if([jsonObject isKindOfClass:[NSDictionary class]]){
                        NSArray *dataArray = [jsonObject objectForKey:@"results"];
                        @autoreleasepool {
                             for (NSDictionary *dict in dataArray) {
                                ItunesSearchResult *result = [[ItunesSearchResult alloc] initWithDictionary:dict];
                                [self.searchResultsArray addObject:result];
                             }
                         }
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if(completionBlock) {
                                completionBlock(self.searchResultsArray);
                            }
                        });
                    } else{
                        // json object is not dictionary
                        NSString* desc = [[NSString alloc] initWithFormat:kHttpRequestErrorDescriptionFormat,
                                          (int)(httpResponse.statusCode),
                                          [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode]];
                        NSError* error = [NSError errorWithDomain:kHttpRequestErrorDomain
                                                             code:kHttpRequestErrorCode
                                                         userInfo:@{NSLocalizedDescriptionKey: desc}];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if(errorBlock) {
                                errorBlock(error);
                            }

                        });
                    }
                } else {
                    NSString* desc = [[NSString alloc] initWithFormat:kHttpRequestErrorDescriptionFormat,
                                      (int)(httpResponse.statusCode),
                                      [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode]];
                    NSError* error = [NSError errorWithDomain:kHttpRequestErrorDomain
                                                         code:kHttpRequestErrorCode
                                                     userInfo:@{NSLocalizedDescriptionKey: desc}];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if(errorBlock) {
                            errorBlock(error);
                        }
                    });
                }
            }  else {
                NSError* error = [NSError errorWithDomain:kHttpRequestErrorDomain
                                                     code:kNoDataErrorCode
                                                 userInfo:@{NSLocalizedDescriptionKey: kNoDataDescription}];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(errorBlock) {
                        errorBlock(error);
                    }

                });
            }
        }
    }];
    [task resume];
    
}


-(void)searchGithubAsynchroniouslyWithSearchTerm:(NSString *)searchTerm withCompletionHandler:(void (^)(NSMutableArray *))completionBlock errorHandler:(void (^)(NSError *))errorBlock {
    
    
    NSString *parameters = @"";
    NSArray *searchTerms = [searchTerm componentsSeparatedByString:@" "];
    for (NSString *string in searchTerms) {
        if([parameters isEqualToString:@""]) {
            parameters = [parameters stringByAppendingString:[NSString stringWithFormat:@"?per_page=100&q=%@",string]];
        } else {
            parameters = [parameters stringByAppendingString:[NSString stringWithFormat:@"+%@",string]];
        }
    }
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kGithubUrl,parameters];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
          
        if(error != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(errorBlock) {
                    errorBlock(error);
                }

            });
        } else {
            if(data) {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                NSRange range = [response.MIMEType rangeOfString:@"application/json"];
                if(httpResponse.statusCode == 200 && range.length != 0) {
                    NSError *error;
                    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                    if([jsonObject isKindOfClass:[NSDictionary class]]){
                        NSArray *dataArray = [jsonObject objectForKey:@"items"];
                        [self.searchResultsArray removeAllObjects];
                        @autoreleasepool {
                            
                            for (NSDictionary *dict in dataArray) {
                                GitHubSearchResult *result = [[GitHubSearchResult alloc] initWithDictionary:dict];
                                [self.searchResultsArray addObject:result];
                                
                            }
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if(completionBlock){
                                completionBlock(self.searchResultsArray);
                            }
                        });
                        
                    } else{
                        // json object is not dictionary
                        NSString* desc = [[NSString alloc] initWithFormat:kHttpRequestErrorDescriptionFormat,
                                          (int)(httpResponse.statusCode),
                                          [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode]];
                        NSError* error = [NSError errorWithDomain:kHttpRequestErrorDomain
                                                             code:kHttpRequestErrorCode
                                                         userInfo:@{NSLocalizedDescriptionKey: desc}];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if(errorBlock) {
                                errorBlock(error);
                            }

                        });
                    }
                } else {
                    NSString* desc = [[NSString alloc] initWithFormat:kHttpRequestErrorDescriptionFormat,
                                      (int)(httpResponse.statusCode),
                                      [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode]];
                    NSError* error = [NSError errorWithDomain:kHttpRequestErrorDomain
                                                         code:kHttpRequestErrorCode
                                                     userInfo:@{NSLocalizedDescriptionKey: desc}];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if(errorBlock) {
                            errorBlock(error);
                        }

                    });
                }
                
            }  else {
                NSError* error = [NSError errorWithDomain:kHttpRequestErrorDomain
                                                     code:kNoDataErrorCode
                                                 userInfo:@{NSLocalizedDescriptionKey: kNoDataDescription}];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(errorBlock) {
                        errorBlock(error);
                    }

                });
            }
            
        }
        
        
    }];
    [task resume];

    
}




@end
