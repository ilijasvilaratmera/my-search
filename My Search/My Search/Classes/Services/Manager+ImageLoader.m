//
//  Manager+ImageLoader.m
//  My Search
//
//  Created by Ilija Svilar on 7/6/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "Manager+ImageLoader.h"

@implementation Manager (ImageLoader)

-(void)loadAsynchroniouslyImageFromUrl:(NSString *)imageUrl withCompletionHandler:(void (^)(UIImage *))completionBlock errorHandler:(void (^)(NSError *))errorBlock {
    
    NSData *imageData = [[Manager sharedInstance].imageCache objectForKey:imageUrl];
    if(!imageData) {
        NSURL* url = [NSURL URLWithString:imageUrl];
        NSURLRequest* request = [NSURLRequest requestWithURL:url];
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDownloadTask *task = [session downloadTaskWithRequest:request completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if (error == nil && httpResponse.statusCode == 200) {
                NSData *data  = [NSData dataWithContentsOfURL:location];
                [[Manager sharedInstance].imageCache setObject:data forKey:imageUrl];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(completionBlock) {
                        completionBlock([UIImage imageWithData:data]);
                    }
                });
            } else {
                if(error == nil) {
                    NSString* desc = [[NSString alloc] initWithFormat:@"HTTP Request failed with status code: %d (%@)",
                                      (int)(httpResponse.statusCode),
                                      [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode]];
                    error = [NSError errorWithDomain:@"HTTP Request"
                                                code:-1100
                                            userInfo:@{NSLocalizedDescriptionKey: desc}];
                }
                if(errorBlock) {
                    errorBlock(error);
                }
            }
        }];
        [task resume];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            completionBlock([UIImage imageWithData:imageData]);
        });
    }
}

@end
