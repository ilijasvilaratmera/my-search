//
//  Manager+Communication.h
//  My Search
//
//  Created by Ilija Svilar on 7/6/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "Manager.h"

@interface Manager (Communication)


-(void)searchItunesAsynchroniouslyWithSearchTerm:(NSString *)searchTerm withCompletionHandler:(void (^)(NSMutableArray *searchResults))completionBlock
                                                                                 errorHandler:(void (^)(NSError *error))errorBlock;


-(void)searchGithubAsynchroniouslyWithSearchTerm:(NSString *)searchTerm withCompletionHandler:(void (^)(NSMutableArray *searchResults))completionBlock
                                                                                 errorHandler:(void (^)(NSError *error))errorBlock;


@end
