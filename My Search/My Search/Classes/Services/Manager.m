//
//  Manager.m
//  My Search
//
//  Created by Ilija Svilar on 7/6/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "Manager.h"

@implementation Manager



+(Manager *)sharedInstance {
    static Manager *sharedObject = nil;
    static dispatch_once_t _singletonPredicate;
    
    dispatch_once(&_singletonPredicate, ^{
        sharedObject = [[super allocWithZone:nil] init];
    });
    
    return sharedObject;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [self sharedInstance];
}

- (id)init {
    self = [super init];
    if(self != nil)
    {
        _imageCache = [[NSCache alloc] init];
        _searchResultsArray = [[NSMutableArray alloc] init];
        
        
    }
    return self;
}

-(void)prepareForTabChange {
    [self.searchResultsArray removeAllObjects];
    [[NSURLSession sharedSession] getAllTasksWithCompletionHandler:^(NSArray<__kindof NSURLSessionTask *> * _Nonnull tasks) {
        for (NSURLSessionTask *task in tasks) {
            [task cancel];
        }
       
    }];


}

@end
