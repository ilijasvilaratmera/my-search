//
//  Manager+ImageLoader.h
//  My Search
//
//  Created by Ilija Svilar on 7/6/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "Manager.h"
@import UIKit;

@interface Manager (ImageLoader)

-(void)loadAsynchroniouslyImageFromUrl:(NSString *)imageUrl withCompletionHandler:(void (^)(UIImage *loadedImage))completionBlock
                                                                     errorHandler:(void (^)(NSError *error))errorBlock;


@end
