//
//  CustomTransitionAnimation.m
//  My Search
//
//  Created by Ilija Svilar on 7/7/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "CustomTransitionAnimation.h"
#import "ImageViewController.h"

@implementation CustomTransitionAnimation

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext {
    return 0.5f;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    // Grab the from and to view controllers from the context
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    ImageViewController *destVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    

    
    // Set our ending frame. We'll modify this later if we have to
    CGRect endFrame = toViewController.view.frame;
    
    if (self.presenting) {
        fromViewController.view.userInteractionEnabled = NO;
        
        [transitionContext.containerView addSubview:fromViewController.view];
        [transitionContext.containerView addSubview:toViewController.view];
        
        UIGraphicsBeginImageContext(fromViewController.view.frame.size);
        [fromViewController.view drawViewHierarchyInRect:fromViewController.view.frame afterScreenUpdates:YES];
        UIGraphicsEndImageContext();
        
        UIView *snapshot = [fromViewController.view resizableSnapshotViewFromRect:self.openingFrame afterScreenUpdates:YES withCapInsets:UIEdgeInsetsZero];
        [transitionContext.containerView addSubview:snapshot];
        snapshot.frame = self.openingFrame;
        
        toViewController.view.alpha = 0.1;
        NSLog(@"opening frame x: %f  y: %f",self.openingFrame.origin.x,self.openingFrame.origin.y);
        [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0.0 usingSpringWithDamping:0.99 initialSpringVelocity:1.0 options:UIViewAnimationOptionAllowAnimatedContent  animations:^{
            snapshot.frame = destVC.fullScreenImageView.frame;
            fromViewController.view.alpha = 0;
            toViewController.view.alpha = 0;
            
            
        } completion:^(BOOL finished) {
            [snapshot removeFromSuperview];
            toViewController.view.alpha = 1.0;
            fromViewController.view.alpha = 1.0;
            [transitionContext completeTransition:YES];
        }];
        
    }
    else {
        //sa snapshot
        toViewController.view.userInteractionEnabled = YES;
        endFrame = self.openingFrame;

        if(toViewController.view.frame.size.height != fromViewController.view.frame.size.height) {
         //   endFrame = [transitionContext.containerView convertRect:self.openingFrame toView:fromViewController.view];
            CGRect f = toViewController.view.frame;

            endFrame = [fromViewController.view convertRect:self.openingFrame toView:toViewController.view];
            
            toViewController.view.frame = CGRectMake(0, 0, f.size.height, f.size.width);
        }
        NSLog(@"opening %f %f",self.openingFrame.origin.x,self.openingFrame.origin.y);
        NSLog(@"end %f %f",endFrame.origin.x,endFrame.origin.y);

        [transitionContext.containerView addSubview:toViewController.view];
        [transitionContext.containerView addSubview:fromViewController.view];
        
        ImageViewController *sdvc = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
        
        UIView *snapshot = [sdvc.view resizableSnapshotViewFromRect:sdvc.fullScreenImageView.frame afterScreenUpdates:YES withCapInsets:UIEdgeInsetsZero];
        [transitionContext.containerView addSubview:snapshot];
        
        
        fromViewController.view.alpha = 0;
        toViewController.view.alpha = 0;
        [UIView animateWithDuration:0 delay:0.0 usingSpringWithDamping:0.99 initialSpringVelocity:1.0 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
            snapshot.frame = endFrame; //self.openingFrame;
            
            toViewController.view.alpha = 1;
        } completion:^(BOOL finished) {
            [snapshot removeFromSuperview];
            toViewController.view.alpha = 1;
            [transitionContext completeTransition:YES];
        }];
        
        
    }
}




@end
