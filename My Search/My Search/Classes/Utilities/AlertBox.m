//
//  AlertBox.m
//  My Search
//
//  Created by Ilija Svilar on 7/10/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "AlertBox.h"


@implementation AlertBox

+(void)showAlertWithMessage:(NSString *)message inViewController:(id)controller {
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alert addAction:ok];
    [controller presentViewController:alert animated:YES completion:nil];
    
}

@end
