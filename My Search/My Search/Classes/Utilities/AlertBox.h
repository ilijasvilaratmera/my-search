//
//  AlertBox.h
//  My Search
//
//  Created by Ilija Svilar on 7/10/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface AlertBox : NSObject

+(void)showAlertWithMessage:(NSString *)message inViewController:(id)controller;

@end
